#include "config.hpp"
#include "ui_iface.hpp"

#include <iostream>
#include <spdlog/common.h>
#include <spdlog/spdlog.h>

#include <boost/program_options.hpp>
using namespace boost::program_options;

int
main(int argc, char* argv[])
{
  std::string log_level = "info";
  options_description desc("Simulator options");
  desc.add_options()("help,h", "print usage message")(
    "log,l", value(&log_level), "Log level, info, warn, error, trace");
  variables_map vm;
  store(parse_command_line(argc, argv, desc), vm);

  if (vm.count("log"))
    log_level = vm["log"].as<std::string>();

  if (log_level == "info")
  {
    spdlog::set_level(spdlog::level::info);
  }
  else if (log_level == "trace")
  {
    spdlog::set_level(spdlog::level::trace);
  }
  else if (log_level == "warn")
  {
    spdlog::set_level(spdlog::level::warn);
  }
  else if (log_level == "error")
  {
    spdlog::set_level(spdlog::level::err);
  }
  else
  {
    spdlog::error("Incorrect log level");
    return 1;
  }

  if (vm.count("help"))
  {
    std::cout << desc << "\n";
    return 0;
  }

  spdlog::info("Current log level: {}", log_level);

  global_config.start_network_processing();

  // this function never returns
  start_gui(argc, argv);
}
