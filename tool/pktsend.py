#!/usr/bin/python
import socket
import argparse
import random

ping_packet = [0,
               15,
               0, 
               1, 2, 3, 4,
               5, 5, 5, 5,
               7, 8, 9, 10]

assign_la_packet = [4,
                    16,
                    0, 
                    15, 
                    1, 2, 3, 4,
                    5, 5, 5, 5,
                    7, 8, 9, 10]

blink_packet = [5,
                14,
                1, 2, 3, 4,
                5, 5, 5, 5,
                7, 8, 9, 10]

const_color_packet = [6,
                      18,
                      0,
                      1, 2, 3, 4,
                      5, 5, 5, 5,
                      7, 8, 9, 10,
                      0xff, 0xff, 0xff]

packets_collection = {"ping" : ping_packet,
                      "assign_la": assign_la_packet,
                      "blink" : blink_packet,
                      "const_color": const_color_packet}


HOST = "127.0.0.1"  # The server's hostname or IP address
PORT_TCP = 5555  # The port used by the server
PORT_UDP = 5556  # The port used by the server

def wrap_with_forward_w_cid(packets):
    forward_w_cid = [0x10, 0x00, 0x00]
    cid = 10
    for packet in packets:
        forward_w_cid.append(cid)
        [forward_w_cid.append(b) for b in packet]

    forward_w_cid_size = len(forward_w_cid)
    forward_w_cid[1] = (forward_w_cid_size&0xff)
    forward_w_cid[2] = (forward_w_cid_size&0xff00)>>2
    return forward_w_cid

def send_packet_tcp(packet, times):
    i = 0
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, PORT_TCP))
        while i < times:
            s.sendall(bytearray(packet))
            print("sending tcp packe...")
            i+=1

def send_packet_udp(packet, times):
    i = 0
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
        while i < times:
            s.sendto(bytearray(packet), (HOST, PORT_UDP))
            print("sending udp packe...")
            i+=1

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Send packet')
    parser.add_argument('-r', '--repeat', type=int, default=1,
                        help='How many times the packet should be send')
    parser.add_argument('-t', '--type', default="ping",
                        help='Packet type. One of: [{}, mixed]'.format(list(packets_collection.keys())))
    parser.add_argument('-m', '--mode', default="tcp",
                        help='Connection type, either TCP or UDP')

    args = parser.parse_args()
    packet = []
    if args.type == "mixed":
        packets_to_shuffle = [ping_packet, const_color_packet, blink_packet, assign_la_packet]
        random.shuffle(packets_to_shuffle)
        packet = wrap_with_forward_w_cid(packets_to_shuffle)
    else:
        packet = wrap_with_forward_w_cid([packets_collection[args.type]])

    if args.mode.lower() == "tcp":
        send_packet_tcp(packet, args.repeat)
    else:
        send_packet_udp(packet, args.repeat)
