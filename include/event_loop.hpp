#pragma once

#include <condition_variable>
#include <mutex>
#include <queue>
#include <thread>
#include <variant>

class event_loop
{
public:
  struct resize_data
  {
    size_t width, height;
  };

  struct finish_loop
  {
  };

  using event_variant = std::variant<resize_data, finish_loop>;

  using queue_t = std::queue<event_variant>;

  void
  send_event(event_variant event);

  event_loop();

  ~event_loop();

private:
  void
  process_events();

  void
  handle_resize(size_t width, size_t height);

  event_variant
  pop_event();

  std::condition_variable _cv;
  std::mutex _mtx;
  std::thread _th;
  queue_t _events;
};
