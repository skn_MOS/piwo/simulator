#pragma once

#include <concepts>
#include <mutex>

template<template<typename> typename L = std::lock_guard,
         typename C,
         typename... Args>
auto
locked_scope(C&& body, Args&&... args) requires std::invocable<C>
{
  L lock(std::forward<Args>(args)...);
  body();
}

template<typename L, typename C, typename... Args>
auto
locked_scope(C&& body, Args&&... args) requires std::invocable<C>
{
  L lock(std::forward<Args>(args)...);
  body();
}

constexpr int SCOPED_LOCK_SUCCESS = -1;
template<typename... Ts>
std::pair<int, std::scoped_lock<std::remove_reference_t<Ts>...>>
scoped_lock_try(Ts&&... args)
{
  return { std::piecewise_construct,
           std::make_tuple(std::try_lock(std::forward<Ts>(args)...)),
           std::forward_as_tuple(std::adopt_lock, std::forward<Ts>(args)...) };
}
