#pragma once
#include <atomic>
#include <boost/asio/io_context.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ip/udp.hpp>
#include <condition_variable>
#include <cstdint>
#include <memory>
#include <mutex>
#include <thread>

#include "network_data_provider.hpp"
#include "suspendable_thread.hpp"

using tcp_socket_t = boost::asio::ip::tcp::socket;
using udp_socket_t = boost::asio::ip::udp::socket;

struct tcp_eth_conntext : public network_data_provider
{
  tcp_eth_conntext(const tcp_eth_conntext&) = delete;
  tcp_eth_conntext(tcp_eth_conntext&& old);
  tcp_eth_conntext(network_id id, tcp_socket_t&& tcp);
  ~tcp_eth_conntext();

  void
  start_receving_no_block();

  auto
  acquire_tcp_socket()
  {
    return std::tuple(std::unique_lock(sock_mtx), std::ref(_tcp_socket));
  }

  network_id
  get_name() const
  {
    return id;
  }

  network_packet_buffer_t
  get_packet()
  {
    std::unique_lock recv_lk(recv_mtx);
    auto packet = tcp_buffer_queue.front();
    tcp_buffer_queue.pop();
    return packet;
  }

  bool
  has_packets()
  {
    std::unique_lock recv_lk(recv_mtx);
    return !tcp_buffer_queue.empty();
  }

  bool
  wait()
  {
    std::unique_lock recv_lk(recv_mtx);
    recv_cv.wait(
      recv_lk,
      [this]() { return !tcp_buffer_queue.empty() || !_tcp_socket.is_open(); });

    return !tcp_buffer_queue.empty() || _tcp_socket.is_open();
  }

  void
  notify()
  {
    std::unique_lock recv_lk(recv_mtx);
    recv_cv.notify_all();
  }

  void
  close();

private:
  const std::atomic<network_id> id;
  // control mutex accessing
  std::recursive_mutex sock_mtx;
  tcp_socket_t _tcp_socket;

  network_packet_queue_t tcp_buffer_queue;
  network_packet_buffer_t tcp_buffer;

  std::mutex recv_mtx;
  std::condition_variable recv_cv;
};

struct udp_eth_conntext : public network_data_provider
{
  void
  start_receving_no_block();

  auto
  acquire_tcp_socket()
  {
    return std::tuple(std::unique_lock(sock_mtx), std::ref(_udp_socket));
  }

  network_id
  get_name() const
  {
    return id;
  }

  network_packet_buffer_t
  get_packet()
  {
    std::unique_lock recv_lk(recv_mtx);
    auto packet = udp_buffer_queue.front();
    udp_buffer_queue.pop();
    return packet;
  }

  bool
  wait()
  {
    std::unique_lock recv_lk(recv_mtx);

    recv_cv.wait(
      recv_lk,
      [this]() { return !udp_buffer_queue.empty() || !_udp_socket.is_open(); });

    return _udp_socket.is_open() || !udp_buffer_queue.empty();
  }

  bool
  has_packets()
  {
    std::unique_lock recv_lk(recv_mtx);
    return !udp_buffer_queue.empty();
  }

  void
  notify()
  {
    std::unique_lock recv_lk(recv_mtx);
    recv_cv.notify_all();
  }

  void
  close();

  udp_eth_conntext(boost::asio::io_context& io_service,
                   boost::asio::ip::udp::endpoint& endpoint,
                   network_id id);
  udp_eth_conntext(udp_socket_t&& udp, network_id id);
  ~udp_eth_conntext();

private:
  const std::atomic<network_id> id;

  std::recursive_mutex sock_mtx;
  udp_socket_t _udp_socket;
  network_packet_queue_t udp_buffer_queue;
  network_packet_buffer_t udp_buffer;
  boost::asio::ip::udp::endpoint sender_endpoint;

  std::mutex recv_mtx;
  std::condition_variable recv_cv;
};

struct network
{
  void
  listen_no_block();
  // This method should be called only on connections that were disconnected
  void
  remove_connection(network_id id);

  network(uint16_t tcp_port, uint16_t udp_port);

  ~network();

  void
  create_udp_connection();

  auto
  acquire_connections()
  {
    return std::make_tuple(std::unique_lock(conn_mtx),
                           std::ref(tcp_connections),
                           std::ref(udp_connection));
  }

  // stop network service
  void
  start();

  // stop network service
  void
  stop();

private:
  void
  handle_traffic();

  /* io service used by network subsystem
   * to handle boost.asio actions
   * used by:
   *  - acceptor to accept new connections
   *  - tcp connection to read packets
   *  - udp connection to read packets
   */
  boost::asio::io_context io_service;
  // used by acceptor to accept new connections
  boost::asio::ip::tcp::endpoint tcp_endpoint;
  boost::asio::ip::udp::endpoint udp_endpoint;
  boost::asio::ip::tcp::acceptor acceptor;

  // contains all incomming tcp connections
  std::recursive_mutex conn_mtx;
  std::vector<std::shared_ptr<tcp_eth_conntext>> tcp_connections;
  // udp socket used to receive all udp traffic
  std::shared_ptr<udp_eth_conntext> udp_connection;

  // unique id assigned to each connection
  network_id unique_connection = 0;

  // thread to process network traffic, both udp/tcp
  suspendable_thread traffic_handler;
};
