#pragma once

#include "common_types.hpp"
#include <piwo/protodef.h>

constexpr int MODULE_INVALID_POS = -1;
struct modinfo_base
{
  int64_t id;
  piwo::uid uid;
};

struct modinfo
{
  modinfo_base base;
  piwo::logic_address_t logic_address;
  piwo::gateway_id_t gateway_id;
  piwo::radio_id_t radio_id;
  piwo::color static_color;
  ipos_t position;
};
