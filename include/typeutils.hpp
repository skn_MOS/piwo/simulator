#pragma once

#include <cstddef>
#include <cstdint>
#include <optional>
#include <string>
#include <vector>

template<class T, class U>
constexpr std::optional<T>
narrow(U u) noexcept(false)
{
  constexpr const bool is_different_signedness =
    (std::is_signed<T>::value != std::is_signed<U>::value);

  const T t = static_cast<T>(u);

  if (static_cast<U>(t) != u ||
      (is_different_signedness && ((t < T{}) != (u < U{}))))
  {
    return std::nullopt;
  }

  return t;
}

template<class T>
bool
is_aligned(const void* ptr) noexcept
{
  // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
  auto iptr = reinterpret_cast<std::uintptr_t>(ptr);
  return !(iptr % alignof(T));
}

template<typename T>
constexpr T*
from_bytes(std::byte* p)
{
  assert(is_aligned<T>(p));

  // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
  return reinterpret_cast<T*>(p);
}

template<typename T>
constexpr const T*
from_bytes(const std::byte* p)
{
  assert(is_aligned<T>(p));

  // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
  return reinterpret_cast<const T*>(p);
}

using audio_list = std::vector<std::string>;
using midi_list = std::vector<std::string>;
