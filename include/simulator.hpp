#pragma once
#include "network_data_provider.hpp"
#include <fmt/format.h>
#include <memory>
#include <piwo/frame.h>
#include <piwo/proto.h>
#include <thread>

#include "frame_provider.hpp"

class simulator : public frame_provider
{
public:
  simulator(simulator&& sim);
  simulator(std::shared_ptr<network_data_provider> provider,
            size_t width,
            size_t height);
  ~simulator();

  bool
  process_piwo_frame(piwo::raw_packet raw);

  frame_provider::id
  get_name() const
  {
    return provider->get_name();
    ;
  }

  piwo::frames_intersection::frame_t
  get_frame_()
  {
    return frame;
  }

  void
  resize_(size_t width, size_t height)
  {
    frame = piwo::alloc_frame_shared(width, height);
  }

  /* Associated render engine step id,
   * set when new step is added to render engine
   */
  std::optional<size_t> pipeline_id;

private:
  void
  process_data_loop();

  piwo::frames_intersection::frame_t frame;
  std::thread proc_th;
  std::shared_ptr<network_data_provider> provider;
};