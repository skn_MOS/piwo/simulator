#pragma once
#include <cassert>
#include <list>
#include <memory>
#include <mutex>
#include <queue>
#include <vector>

using network_packet_buffer_t = std::basic_string<std::byte>;
using network_packet_queue_t =
  std::queue<network_packet_buffer_t, std::list<network_packet_buffer_t>>;

using network_id = uint64_t;

class network_data_provider
{
public:
  network_data_provider(bool is_waitable)
    : is_waitable(is_waitable)
  {
  }

  virtual ~network_data_provider() = default;

  virtual  network_id
  get_name() const = 0;

  virtual network_packet_buffer_t
  get_packet() = 0;

  virtual bool
  has_packets() = 0;

  virtual void
  notify() = 0;

  bool
  waitable()
  {
    return is_waitable;
  }

  virtual bool
  wait()
  {
    assert(false && "Frame provider does not implement the wait function");
    return false;
  }

protected:
  bool is_waitable;
};
