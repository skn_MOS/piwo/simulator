#pragma once
#include "config.hpp"
#include <piwo/proto.h>

void
start_gui(int argc, char* argv[]);

void
config_module_added(const piwo::uid uid);

void
config_module_removed(const piwo::uid uid);

void
config_module_data_changed(const piwo::uid uid);

void
modules_config_container_rearanged();

void
config_resolution_changed();

void
light_show_stopped();

void
light_show_started();

void
application_state_changed(const application_state_t state);
