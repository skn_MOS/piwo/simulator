#pragma once
#include <atomic>
#include <chrono>
#include <condition_variable>
#include <cstddef>
#include <cstdlib>
#include <functional>
#include <memory>
#include <mutex>
#include <optional>
#include <piwo/common_intersection_operators.h>
#include <spdlog/spdlog.h>
#include <vector>

#include <mipc/timer.h>
#include <piwo/frame.h>
#include <piwo/frames_intersection.h>

#include "frame_provider.hpp"
#include "lock.hpp"
#include "network_data_provider.hpp"
#include "suspendable_thread.hpp"

struct render_engine_step
{
  static inline size_t next_id = 0;
  friend class render_engine;

private:
  render_engine_step(std::weak_ptr<frame_provider> fp1_w,
                     std::weak_ptr<frame_provider> fp2_w,
                     const piwo::frames_intersection::frame_t& f1,
                     const piwo::frames_intersection::frame_t& f2,
                     int64_t offx,
                     int64_t offy,
                     piwo::frames_intersection_operator e)
    : id(next_id++)
    , off_x(offx)
    , off_y(offy)
    , active(true)
    , effect(e)
    , _intersection(f2, f1, offx, offy)
    , _source_provider(std::move(fp1_w))
    , _destination_provider(std::move(fp2_w))
  {
  }

public:
  static std::optional<render_engine_step>
  make_step(std::weak_ptr<frame_provider> fp1_w,
            std::weak_ptr<frame_provider> fp2_w,
            int64_t offx,
            int64_t offy,
            piwo::frames_intersection_operator e)
  {
    auto fp1 = fp1_w.lock();
    if (!fp1)
      return std::nullopt;

    auto fp2 = fp2_w.lock();
    if (!fp2)
      return std::nullopt;

    return render_engine_step(
      fp1, fp2, fp1->get_frame(), fp2->get_frame(), offx, offy, e);
  }

  bool
  operator==(const size_t id)
  {
    return this->id == id;
  }

  bool
  refresh()
  {
    auto fp1 = _source_provider.lock();
    if (!fp1)
      return false;

    auto fp2 = _destination_provider.lock();
    if (!fp2)
      return false;

    this->_intersection = piwo::frames_intersection(
      fp2->get_frame(), fp1->get_frame(), off_x, off_y);
    return true;
  }

  size_t id;
  int64_t off_x, off_y;
  bool active;
  piwo::frames_intersection_operator effect;

private:
  piwo::frames_intersection _intersection;
  std::weak_ptr<frame_provider> _source_provider, _destination_provider;
};

class render_engine : public frame_provider
{
  static constexpr auto SEC = std::chrono::seconds{ 0 };
  static constexpr auto MILLI = std::chrono::milliseconds{ 50 };

public:
  enum class state_t
  {
    re_busy,
    re_failed,
    re_ok,
  };

  render_engine(size_t rows, size_t cols)
    : frame_provider(true)
    , _th(&render_engine::render, std::ref(*this))
  {
    this->_frame = piwo::alloc_frame(rows, cols);
  }

  ~render_engine() { this->stop(); }

  void
  stop()
  {
    this->_tim.stop();
    this->_th.stop_and_wait();
  }

  void
  start()
  {
    this->start(this->_sec, this->_mili);
  }

  void
  start(const std::chrono::seconds& sec, const std::chrono::milliseconds& mili)
  {
    locked_scope(
      [&]
      {
        if (this->_render_active)
          return;

        this->_render_active = true;
      },
      this->_mtx);

    this->_th.resume();
    this->_sec = sec;
    this->_mili = mili;
    this->_tim.start(sec, mili);
  }

  [[nodiscard]] state_t
  blocking_add(const render_engine_step& config)
  {
    this->stop();
    return this->add_step(config);
  }

  bool
  is_active()
  {
    return this->_tim.is_active();
  }

  [[nodiscard]] state_t
  blocking_remove(frame_provider::id id)
  {
    this->stop();
    return this->remove_step(id);
  }

  [[nodiscard]] state_t
  add_step(render_engine_step config)
  {
    if (this->_render_active == true)
    {
      spdlog::warn("Adding step failed. Render engine busy");
      return state_t::re_busy;
    }
    this->_steps.push_back(config);
    return state_t::re_ok;
  }

  [[nodiscard]] state_t
  remove_step(frame_provider::id id)
  {
    if (this->_render_active == true)
    {
      spdlog::warn("Step removal failed. Render engine busy");
      return state_t::re_busy;
    }

    auto step = std::find_if(this->_steps.begin(),
                             this->_steps.end(),
                             [id](const render_engine_step& step)
                             { return step.id == id; });

    if (step == this->_steps.end())
    {
      spdlog::warn(
        "Step removal failed. Configuration with specified id does not exist");
      return state_t::re_failed;
    }

    this->_steps.erase(step);
    return state_t::re_ok;
  }

  [[nodiscard]] state_t
  refresh_step(size_t id)
  {
    if (this->_render_active == true)
    {
      spdlog::warn("Refreshing step failed. Render engine busy");
      return state_t::re_busy;
    }

    auto step = std::find(this->_steps.begin(), this->_steps.end(), id);
    if (step == this->_steps.end())
    {
      spdlog::warn(
        "Step removal failed. Configuration with specified id does not exist");
      return state_t::re_failed;
    }

    if (step->refresh() == false)
      return state_t::re_failed;

    return state_t::re_ok;
  }

  void
  render()
  {
    if (!this->_tim.wait())
    {
      locked_scope(
        [&]
        {
          this->_render_active = false;
          this->_cv.notify_all();
        },
        this->_mtx);

      return;
    }
    locked_scope(
      [&]
      {
        this->_frame->fill({ 0, 0, 0 });
        for (const auto& step : this->_steps)
        {
          std::invoke(step.effect, step._intersection);
        }
        this->_rendered = true;
        this->_cv.notify_all();
      },
      this->_mtx);
  }

  piwo::frames_intersection::frame_t
  get_frame_() override
  {
    return this->_frame;
  }

  void
  resize_(size_t width, size_t height) override
  {
    spdlog::info("Changing frame resolution of\
        render engine to width->{} height->{}",
                 width,
                 height);
    this->_frame = piwo::alloc_frame_shared(width, height);
  }

  frame_provider::id
  get_name() const override
  {
    // TODO, add supervisor to set proper ids for each frame provider
    // object
    return 0xfffff;
  }

  void
  wait() override
  {
    std::unique_lock lk(this->_mtx);
    this->_cv.wait(lk,
                   [this] { return this->_rendered || !this->_render_active; });
    this->_rendered = false;
  }

  auto&
  get_configuration()
  {
    return this->_steps;
  }

private:
  std::vector<render_engine_step> _steps;
  std::shared_ptr<piwo::frame> _frame;
  std::condition_variable _cv;
  std::mutex _mtx;
  mipc::timer _tim;
  suspendable_thread _th;
  bool _rendered = false;
  std::atomic_bool _render_active = false;

  std::chrono::seconds _sec = SEC;
  std::chrono::milliseconds _mili = MILLI;
};
