#pragma once
#include "modinfo.hpp"
#include "network.hpp"
#include "simulator.hpp"
#include <algorithm>
#include <memory>
#include <mutex>
#include <set>
#include <spdlog/spdlog.h>
#include <string_view>
#include <tuple>

#include "event_loop.hpp"
#include "modinfo.hpp"
#include "render_engine.hpp"

constexpr int udp_port = 5556;
constexpr int tcp_port = 5555;

enum class application_state_t
{
  IDLE = 0,
  DISPLAY_AUTO_CONFIG,
  LIGHTSHOW
};

class config_t
{
  using uresolution_t = resolution_t<size_t>;
  struct modinfo_set_comparator
  {
    bool
    operator()(const modinfo& lhs, const modinfo& rhs) const
    {
      return lhs.base.uid.packed() < rhs.base.uid.packed();
    }
  };

  using modinfo_t = std::set<modinfo, modinfo_set_comparator>;

public:
  config_t();
  ~config_t();

  auto
  acquire_network()
  {
    return std::tuple(std::unique_lock(network_mtx), std::ref(net));
  }

  void
  start_network_processing();

  void
  stop_network_processing();

  bool
  create_new_sim_unit(std::shared_ptr<network_data_provider> provider);

  void
  remove_connection(network_id id);

  application_state_t
  get_application_state();

  auto
  acquire_resolution()
  {
    return std::make_tuple(std::unique_lock(this->resolution_mtx),
                           std::ref(resolution));
  }

  bool
  disable_light_show();

  bool
  enable_light_show();

  auto
  acquire_modules()
  {
    return std::make_tuple(std::unique_lock(this->modules_mtx),
                           std::ref(modules));
  }

  void
  load_from_file(std::string_view path);

  void
  set_resolution(uresolution_t::underlying_type width,
                 uresolution_t::underlying_type height);

  void
  assign_module_position(const piwo::uid& uid, ipos_t& position);

  void
  assign_logic_address(const piwo::uid& uid, uint8_t logic_address);

  auto
  acquire_simulators()
  {
    return std::make_tuple(std::unique_lock(this->sim_mtx),
                           std::ref(simulators));
  }

  auto
  acquire_render_engine()
  {
    return std::make_tuple(std::unique_lock(this->re_mtx), std::ref(re));
  }

private:
  std::recursive_mutex sim_mtx;
  std::vector<std::shared_ptr<simulator>> simulators;

  std::recursive_mutex re_mtx;
  std::shared_ptr<render_engine> re;

  std::recursive_mutex network_mtx;
  network net;

  std::recursive_mutex modules_mtx;
  modinfo_t modules;

  std::recursive_mutex resolution_mtx;
  uresolution_t resolution{ .width = 0, .height = 0 };

  std::recursive_mutex application_state_mtx;
  application_state_t application_state;

  std::shared_ptr<spdlog::logger> logger;

  event_loop _event_loop;
};

inline config_t global_config;
