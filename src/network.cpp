#include "network.hpp"

#include "config.hpp"
#include "lock.hpp"
#include <algorithm>
#include <atomic>
#include <memory>
#include <queue>
#include <spdlog/spdlog.h>

using boost::asio::ip::udp;

void
network::listen_no_block()
{
  acceptor.listen();
  acceptor.async_accept(
    [this](const boost::system::error_code& error,
           boost::asio::ip::tcp::socket peer)
    {
      if (error)
      {
        spdlog::error("Failed when accepting the connection {}",
                      error.message());
        return;
      }
      auto& new_conn =
        tcp_connections.emplace_back(std::make_shared<tcp_eth_conntext>(
          unique_connection++, std::move(peer)));
      global_config.create_new_sim_unit(new_conn);
      listen_no_block();
    });
}

void
network::remove_connection(network_id id)
{
  auto conn_to_remove =
    std::find_if(tcp_connections.begin(),
                 tcp_connections.end(),
                 [id](const std::shared_ptr<tcp_eth_conntext>& conn)
                 { return conn->get_name() == id; });
  if (conn_to_remove == tcp_connections.end())
  {
    return;
  }
  auto [mtx_guard, sock] = (*conn_to_remove)->acquire_tcp_socket();
  if (sock.get().is_open())
  {
    spdlog::error("Removing socket can be done only on incactive connections");
    return;
  }
  tcp_connections.erase(conn_to_remove);
  spdlog::trace("Number of active connections {}", tcp_connections.size());
}

network::network(uint16_t tcp_port, uint16_t udp_port)
  : tcp_endpoint(boost::asio::ip::tcp::v4(), tcp_port)
  , udp_endpoint(boost::asio::ip::udp::v4(), udp_port)
  , acceptor(io_service, boost::asio::ip::tcp::v4())
  , traffic_handler(&network::handle_traffic, std::ref(*this))

{
  acceptor.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
  acceptor.bind(tcp_endpoint);
}

void
network::create_udp_connection()
{
  udp_connection = std::make_shared<udp_eth_conntext>(
    io_service, udp_endpoint, unique_connection++);
  if (global_config.create_new_sim_unit(udp_connection) == false)
  {
    spdlog::error("Creating sim unit for udp failed");
  }
}

network::~network()
{
  this->stop();
}

void
network::start()
{
  listen_no_block();
  traffic_handler.resume();
}

void
network::stop()
{
  udp_connection->close();
  for (auto c : tcp_connections)
    c->close();

  io_service.stop();
  traffic_handler.stop();
}

void
network::handle_traffic()
{
  io_service.run();
}
