#include "network.hpp"

#include "config.hpp"
#include "lock.hpp"
#include <algorithm>
#include <atomic>
#include <queue>
#include <spdlog/spdlog.h>

tcp_eth_conntext::tcp_eth_conntext(tcp_eth_conntext&& old)
  : network_data_provider(old.is_waitable)
  , id(old.id.load())
  , _tcp_socket(std::move(old._tcp_socket))
  , tcp_buffer_queue(std::move(old.tcp_buffer_queue))
  , tcp_buffer(std::move(old.tcp_buffer))
{
  old.recv_cv.notify_all();

  locked_scope(
    [this]
    {
      _tcp_socket.cancel();
      if (_tcp_socket.is_open())
        start_receving_no_block();
    },
    sock_mtx);
}

tcp_eth_conntext::tcp_eth_conntext(uint64_t id, tcp_socket_t&& tcp)
  : network_data_provider(true)
  , id(id)
  , _tcp_socket(std::move(tcp))
{
  locked_scope(
    [this]
    {
      tcp_buffer.reserve(1024);
      _tcp_socket.cancel();
      if (_tcp_socket.is_open())
        start_receving_no_block();
    },
    sock_mtx);
}

void
tcp_eth_conntext::start_receving_no_block()
{
  std::unique_lock lck(sock_mtx);
  if (!_tcp_socket.is_open())
    return;

  _tcp_socket.async_receive(
    boost::asio::buffer(tcp_buffer.data(), tcp_buffer.capacity()),
    [this](const boost::system::error_code& error,
           [[maybe_unused]] std::size_t bytes_received)
    {
      if ((boost::asio::error::eof == error) ||
          (boost::asio::error::connection_reset == error))
      {
        locked_scope(
          [this]
          {
            _tcp_socket.close();
            recv_cv.notify_all();
          },
          sock_mtx);
        // after remove_connection call tcp object is invalid
        // and any access to *this pointer is forbidden
        global_config.remove_connection(id);
        spdlog::trace("Client disconnected");
        return;
      }

      if (error)
      {
        locked_scope(
          [this]
          {
            _tcp_socket.close();
            recv_cv.notify_all();
          },
          sock_mtx);
        spdlog::error("Error while receiving: {}", error.message());
        return;
      }

      locked_scope(
        [this, bytes_received]
        {
          tcp_buffer_queue.emplace(tcp_buffer.data(), bytes_received);
          recv_cv.notify_all();
          start_receving_no_block();
        },
        recv_mtx);
    });
}

void
tcp_eth_conntext::close()
{
  locked_scope(
    [this]
    {
      if (_tcp_socket.is_open())
        _tcp_socket.close();
      recv_cv.notify_all();
    },
    sock_mtx);

  locked_scope([this] { this->recv_cv.notify_all(); }, recv_mtx);
}

tcp_eth_conntext::~tcp_eth_conntext()
{
  recv_cv.notify_all();
}