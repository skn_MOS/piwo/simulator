#include "lock.hpp"
#include "network.hpp"
#include <spdlog/spdlog.h>

#include "config.hpp"

udp_eth_conntext::udp_eth_conntext(boost::asio::io_context& io_service,
                                   boost::asio::ip::udp::endpoint& endpoint,
                                   network_id id)
  : network_data_provider(true)
  , id(id)
  , _udp_socket(io_service, endpoint)

{
  locked_scope(
    [this]
    {
      udp_buffer.resize(1024);
      if (_udp_socket.is_open())
        start_receving_no_block();
    },
    sock_mtx);
}

udp_eth_conntext::udp_eth_conntext(udp_socket_t&& udp,
                                   network_id id)
  : network_data_provider(true)
  , id(id)
  , _udp_socket(std::move(udp))
{
  locked_scope(
    [this]
    {
      udp_buffer.resize(1024);
      _udp_socket.cancel();
      if (_udp_socket.is_open())
        start_receving_no_block();
    },
    sock_mtx);
}

void
udp_eth_conntext::start_receving_no_block()
{
  std::unique_lock lck(sock_mtx);
  if (!_udp_socket.is_open())
    return;

  _udp_socket.async_receive_from(
    boost::asio::buffer(udp_buffer.data(), udp_buffer.size()),
    sender_endpoint,
    [this](const boost::system::error_code& error,
           [[maybe_unused]] std::size_t bytes_received)
    {
      if (error)
      {
        _udp_socket.close();
        recv_cv.notify_all();
        spdlog::error("Error while receiving: {}", error.message());
        return;
      }

      std::unique_lock recv_lk(recv_mtx);
      udp_buffer_queue.emplace(udp_buffer.data(), bytes_received);
      recv_cv.notify_all();
      start_receving_no_block();
    });
}

void
udp_eth_conntext::close()
{
  locked_scope(
    [this]
    {
      if (_udp_socket.is_open())
        _udp_socket.close();
      recv_cv.notify_all();
    },
    sock_mtx);
  locked_scope([this] { this->recv_cv.notify_all(); }, recv_mtx);
}

udp_eth_conntext::~udp_eth_conntext()
{
  recv_cv.notify_all();
}
