#include "modinfo.hpp"
#include "json.hpp"
#include "typeutils.hpp"

template<>
void
serialize(ipos_t& pos, serialization_writer& writer)
{
  writer.StartObject();
  writer.String("x");
  writer.Int(pos.x);

  writer.String("y");
  writer.Int(pos.y);
  writer.EndObject();
}

template<>
void
serialize(upos_t& pos, serialization_writer& writer)
{
  writer.StartObject();
  writer.String("x");
  writer.Uint64(pos.x);

  writer.String("y");
  writer.Uint64(pos.y);
  writer.EndObject();
}

template<>
bool
deserialize(ipos_t& pos,
            json_object d) // NOLINT(performance-unnecessary-value-param)
{
  pos.x = d["position"].FindMember("x")->value.GetInt();
  pos.y = d["position"].FindMember("y")->value.GetInt();

  return true;
}

template<>
bool
deserialize(upos_t& pos,
            json_object d) // NOLINT(performance-unnecessary-value-param)
{
  pos.x = d["position"].FindMember("x")->value.GetUint64();
  pos.y = d["position"].FindMember("y")->value.GetUint64();

  return true;
}

template<>
void
serialize(modinfo_base& mi, serialization_writer& writer)
{
  writer.StartObject();

  writer.String("id");
  writer.Int64(mi.id);

  writer.String("uid");
  writer.String(mi.uid.to_ascii().c_str());

  writer.EndObject();
}

template<>
bool
deserialize(modinfo_base& mi,
            json_object d) // NOLINT(performance-unnecessary-value-param)
{
  mi.id = d["id"].GetInt64();

  if (std::optional uid_opt = piwo::uid::from_ascii(d["uid"].GetString());
      !uid_opt.has_value())
    return false;
  else
    mi.uid = *uid_opt;

  return true;
}

template<>
void
serialize(modinfo& mi, serialization_writer& writer)
{
  writer.StartObject();

  writer.String("base");
  serialize(mi.base, writer);

  writer.String("logic_address");
  writer.Uint(mi.logic_address);

  writer.String("gateway_id");
  writer.Uint(mi.gateway_id);

  writer.String("radio_id");
  writer.Uint(mi.radio_id);

  writer.String("static_color");
  writer.Uint64(mi.static_color.convert<uint64_t>());

  writer.String("position");
  serialize(mi.position, writer);

  writer.EndObject();
}

template<>
bool
deserialize(modinfo& mi,
            json_object d) // NOLINT(performance-unnecessary-value-param)
{
  json_object dbase = d["base"].GetObject();
  deserialize(mi.base, dbase);

  if (std::optional la_opt = narrow<uint8_t>(d["logic_address"].GetUint());
      !la_opt.has_value())
    return false;
  else
    mi.logic_address = *la_opt;

  if (std::optional gid_opt = narrow<uint8_t>(d["gateway_id"].GetUint());
      !gid_opt.has_value())
    return false;
  else
    mi.gateway_id = *gid_opt;

  if (std::optional rid_opt = narrow<uint8_t>(d["radio_id"].GetUint());
      !rid_opt.has_value())
    return false;
  else
    mi.radio_id = *rid_opt;

  mi.static_color = d["static_color"].GetUint64();

  deserialize(mi.position, d);

  return true;
}
