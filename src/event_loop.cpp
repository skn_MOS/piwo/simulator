#include "event_loop.hpp"
#include "config.hpp"
#include "render_engine.hpp"
#include <chrono>
#include <piwo/common_intersection_operators.h>
#include <piwo/frames_intersection.h>

template<class... Ts>
struct overloaded : Ts...
{
  using Ts::operator()...;
};
template<class... Ts>
overloaded(Ts...) -> overloaded<Ts...>;

void
event_loop::send_event(event_variant event)
{
  locked_scope(
    [&]
    {
      this->_events.push(event);
      this->_cv.notify_all();
    },
    this->_mtx);
}

event_loop::event_loop()
{
  this->_th = std::thread(&event_loop::process_events, std::ref(*this));
}

event_loop::~event_loop()
{
  locked_scope(
    [&]
    {
      this->_events = queue_t();
      this->_events.push(finish_loop());
      this->_cv.notify_all();
    },
    this->_mtx);

  if (this->_th.joinable())
    this->_th.join();
}

event_loop::event_variant
event_loop::pop_event()
{
  std::unique_lock<std::mutex> lk(this->_mtx);
  this->_cv.wait(lk, [this]() { return (this->_events.size() > 0); });

  event_variant ev = this->_events.front();
  this->_events.pop();
  return ev;
}

void
event_loop::process_events()
{
  bool is_alive = true;
  while (is_alive)
  {
    event_variant ev = this->pop_event();
    std::visit(
      overloaded{
        [this](resize_data& data)
        {
          auto [width, height] = data;
          handle_resize(width, height);
        },
        [&is_alive](finish_loop&) { is_alive = false; },
      },
      ev);
  }
}

void
event_loop::handle_resize(size_t width, size_t height)
{
  // TODO take application state mutex
  // TODO remove requiremnt for two mutexes to be taken at once
  // possible deadlock
  auto [mtx, simulators] = global_config.acquire_simulators();
  auto [re_mtx, re] = global_config.acquire_render_engine();
  re->resize(width, height);
  for (const auto& sim : simulators)
  {
    sim->resize(width, height);
    if (sim->pipeline_id.has_value())
    {
      auto res = re->refresh_step(sim->pipeline_id.value());
      if (res != render_engine::state_t::re_ok)
      {
        spdlog::error("Refreshing step failed {}", sim->pipeline_id.value());
      }
      spdlog::info("Refreshing step success {} {}",
                   sim->get_name(),
                   sim->pipeline_id.value());
    }
    else
    {
      spdlog::error(" Pipeline id doesn't exist for sim {}", sim->get_name());
    }
  }
}
