#include "simulator.hpp"
#include "config.hpp"
#include "frame_provider.hpp"
#include <piwo/color.h>
#include <piwo/frame.h>
#include <piwo/proto.h>
#include <piwo/protodef.h>
#include <spdlog/spdlog.h>
#include <thread>
#include <unistd.h>

simulator::simulator(std::shared_ptr<network_data_provider> provider,
                     size_t width,
                     size_t height)
  : frame_provider(true)
  , provider(std::move(provider))
{
  proc_th = std::thread(&simulator::process_data_loop, this);
  frame = piwo::alloc_frame(width, height);
}

bool
simulator::process_piwo_frame(piwo::raw_packet raw)
{
  if (!raw.get_type().has_value())
    return false;

  switch (raw.get_type().value())
  {
    case piwo::packet_type::PING:
    {
      auto ping_opt = piwo::ping::make_ping(raw);
      if (!ping_opt.has_value())
      {
        spdlog::trace("Incomplete packet");
        return false;
      }
      [[maybe_unused]] auto ping = ping_opt.value();
    }
    break;
    case piwo::packet_type::ASSIGN_LOGIC_ADDRESS:
    {
      auto la_opt = piwo::assign_la::make_assign_la(raw);
      if (!la_opt.has_value())
      {
        spdlog::error("Incomplete packet");
        return false;
      }
      auto la = la_opt.value();
      global_config.assign_logic_address(la.get_uid(), la.get_la());
    }
    break;
    case piwo::packet_type::BLINK:
    {
      auto blink_opt = piwo::blink::make_blink(raw);
      if (!blink_opt.has_value())
      {
        spdlog::error("Incomplete packet");
        return false;
      }
      [[maybe_unused]] auto blink = blink_opt.value();
    }
    break;
    case piwo::packet_type::CONST_COLOR:
    {
      auto cc_opt = piwo::constant_color::make_constant_color(raw);
      if (!cc_opt.has_value())
      {
        spdlog::error("Incomplete packet");
        return false;
      }
      auto cc = cc_opt.value();
      auto [mtx, modules] = global_config.acquire_modules();

      modinfo key{};
      key.base.uid = cc.get_uid();
      auto mod_iter = modules.find(key);
      if (mod_iter == modules.end())
        return false;
      auto pos = mod_iter->position;
      auto color = cc.get_color();
      piwo::color c(color.get_red(), color.get_green(), color.get_blue());

      assert(frame->height != 0 && frame->width != 0);
      this->frame->at_(pos.x, frame->height - pos.y - 1) = c;

      spdlog::trace(
        "{} {} {}", color.get_red(), color.get_green(), color.get_blue());
    }
    break;
    case piwo::packet_type::LIGHT_SHOW:
    {
      auto lightshow_opt = piwo::lightshow::make_lightshow(raw);
      if (!lightshow_opt.has_value())
      {
        spdlog::error("Incomplete packet");
        return false;
      }
      auto lightshow = lightshow_opt.value();
      auto [mtx, modules] = global_config.acquire_modules();

      for (uint8_t i = 0; i < lightshow.get_colors_count(); ++i)
      {
        uint8_t la = lightshow.get_first_la() + i;
        auto mod =
          std::find_if(modules.begin(),
                       modules.end(),
                       [la](modinfo m)
                       { return m.logic_address == la && m.position.x != -1; });
        if (mod == modules.end())
          continue;

        auto color = lightshow.get_color(i);
        if (!color.has_value())
          continue;

        piwo::color c(color.value().get_red(),
                      color.value().get_green(),
                      color.value().get_blue());

        auto pos = mod->position;
        assert(frame->height != 0 && frame->width != 0);
        this->frame->at_(pos.x, frame->height - pos.y - 1) = c;
      }
    }
    break;
    default:
      return false;
      break;
  }

  return true;
}

void
simulator::process_data_loop()
{
  piwo::packet_type piwo_packet_type;
  network_packet_buffer_t buffor;
  size_t processed_bytes = 0;
  size_t handled_packets = 0;
  std::byte* buffor_data_ptr;
  size_t data_left_size = 0;

  spdlog::trace("Enetring loop");

  while (provider->wait())
  {
    buffor.append(provider->get_packet());
    while (processed_bytes < buffor.size())
    {
      buffor_data_ptr = buffor.data() + processed_bytes;
      piwo_packet_type =
        static_cast<piwo::packet_type>(buffor_data_ptr[piwo::common_type_pos]);

      data_left_size = buffor.size() - processed_bytes;
      switch (piwo_packet_type)
      {
        case piwo::packet_type::FORWARD_W_CID:
        {
          piwo::raw_packet raw_packet(buffor_data_ptr, data_left_size);
          auto forward_w_cid_opt =
            piwo::forward_w_cid::make_forward_w_cid(raw_packet);
          if (!forward_w_cid_opt.has_value())
          {
            std::copy(
              buffor.begin() + processed_bytes, buffor.end(), buffor.begin());
            buffor.resize(data_left_size);
            spdlog::error("Incomplete packet");
            goto incorect_packet;
          }
          auto forward_w_cid = forward_w_cid_opt.value();
          while (true)
          {
            auto packet_opt = forward_w_cid.get_next_packet();
            if (!packet_opt.has_value())
              break;

            if (process_piwo_frame(packet_opt.value().packet))
              spdlog::info("[ {} ] Processing piwo packet successed",
                           handled_packets++);
          }
          processed_bytes += forward_w_cid.size();
        }
        break;
        default:
          // incorrect byte, read next one
          spdlog::error("All packets should be wrapped in forward_w_cid packet",
                        handled_packets++);
          processed_bytes++;
          break;
      }
    }
    buffor.clear();
  incorect_packet:
    processed_bytes = 0;
  }
  spdlog::trace("Exiting loop");
}

simulator::simulator(simulator&& old)
  : frame_provider(old.is_waitable)
{
  if (old.proc_th.joinable())
    old.proc_th.join();
  proc_th = std::thread(&simulator::process_data_loop, this);
  provider = std::move(old.provider);
}

simulator::~simulator()
{
  do
  {
    this->provider->notify();
  } while (this->provider->has_packets());

  if (proc_th.joinable())
    proc_th.join();
}
