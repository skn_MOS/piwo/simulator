#include "config.hpp"
#include "json.hpp"
#include "lock.hpp"
#include "render_engine.hpp"
#include "ui_iface.hpp"
#include <fstream>
#include <memory>
#include <piwo/common_intersection_operators.h>
#include <piwo/frames_intersection.h>
#include <rapidjson/document.h>
#include <rapidjson/istreamwrapper.h>
#include <spdlog/spdlog.h>

void
config_t::start_network_processing()
{
  locked_scope([this] { net.start(); }, network_mtx);
}

void
config_t::stop_network_processing()
{
  locked_scope([this] { net.stop(); }, network_mtx);
}

application_state_t
config_t::get_application_state()
{
  return this->application_state;
}

bool
config_t::create_new_sim_unit(std::shared_ptr<network_data_provider> provider)
{
  auto [lck_status, lck] =
    scoped_lock_try(re_mtx, application_state_mtx, sim_mtx, resolution_mtx);
  auto app_state_cpy = this->application_state;

  if (lck_status != SCOPED_LOCK_SUCCESS)
  {
    spdlog::warn("Some other operation is being taken on player. Lightshow "
                 "hasn't been disabled!");
    return false;
  }

  if (app_state_cpy != application_state_t::IDLE)
    disable_light_show();

  auto& sim = simulators.emplace_back(
    std::make_shared<simulator>(provider, resolution.width, resolution.height));

  auto re_step_opt =
    render_engine_step::make_step(sim, re, 0, 0, piwo::frame_or_op);
  if (!re_step_opt.has_value())
    return false;

  auto state = re->add_step(re_step_opt.value());

  if (state != render_engine::state_t::re_ok)
  {
    spdlog::trace("Adding new step to render engine failed");
    return false;
  }
  sim->pipeline_id.emplace(re_step_opt.value().id);

  if (app_state_cpy == application_state_t::LIGHTSHOW)
    enable_light_show();
  return true;
}

void
config_t::assign_logic_address(const piwo::uid& uid, uint8_t logic_address)
{
  auto [lck_status, lck] = scoped_lock_try(application_state_mtx, modules_mtx);
  if (lck_status != SCOPED_LOCK_SUCCESS)
  {
    spdlog::warn("Some other operation is being taken on player. Lightshow "
                 "hasn't been disabled!");
    return;
  }

  if (this->application_state == application_state_t::LIGHTSHOW)
  {
    spdlog::warn("Player is in LIGHTSHOW state. Rejecting "
                 "logic address configuration!");
    return;
  }

  modinfo key{};
  key.base.uid = uid;
  auto module = this->modules.extract(key);
  if (!module)
  {
    spdlog::critical("This should never happen");
    return;
  }
  module.value().logic_address = logic_address;

  this->modules.insert(std::move(module));
}

void
config_t::remove_connection(network_id id)
{
  auto [lck_status, lck] =
    scoped_lock_try(re_mtx, application_state_mtx, sim_mtx, network_mtx);
  auto app_state_cpy = this->application_state;
  if (lck_status != SCOPED_LOCK_SUCCESS)
  {
    spdlog::warn("Some other operation is being taken on player. Lightshow "
                 "hasn't been disabled!");
    return;
  }

  if (app_state_cpy != application_state_t::IDLE)
    disable_light_show();

  net.remove_connection(id);

  auto sim_iter = std::find_if(simulators.begin(),
                               simulators.end(),
                               [id](const std::shared_ptr<simulator>& sim)
                               { return sim->get_name() == id; });
  if (sim_iter == simulators.end())
  {
    spdlog::error("Can't remove simulator");
    return;
  }
  if (!sim_iter->get()->pipeline_id.has_value())
  {
    spdlog::error("Simulator should has the step id attached!");
    return;
  }

  auto re_state = re->remove_step(sim_iter->get()->pipeline_id.value());
  if (re_state != render_engine::state_t::re_ok)
  {
    spdlog::trace("Removing state from render engine failed");
    return;
  }

  simulators.erase(sim_iter);
  spdlog::trace("Removing simulator done");

  if (app_state_cpy == application_state_t::LIGHTSHOW)
    enable_light_show();
}

bool
config_t::disable_light_show()
{
  spdlog::trace(__PRETTY_FUNCTION__);
  auto [lck_status, lck] = scoped_lock_try(re_mtx, application_state_mtx);

  if (lck_status != SCOPED_LOCK_SUCCESS)
  {
    spdlog::warn("Some other operation is being taken on player. Lightshow "
                 "hasn't been disabled!");
    return false;
  }

  if (this->application_state != application_state_t::LIGHTSHOW)
  {
    spdlog::warn("Player is not in LIGHTSHOW state. Rejecting disable "
                 "lightshow request!");
    return false;
  }

  application_state = application_state_t::IDLE;

  spdlog::info("Lightshow disabled");
  application_state_changed(this->application_state);

  re->stop();
  light_show_stopped();

  return true;
}

bool
config_t::enable_light_show()
{
  spdlog::trace(__PRETTY_FUNCTION__);

  auto [lck_status, lck] = scoped_lock_try(re_mtx, application_state_mtx);

  if (lck_status != SCOPED_LOCK_SUCCESS)
  {
    spdlog::warn("Some other operation is being taken on player. Lightshow "
                 "hasn't been enabled!");
    return false;
  }

  if (application_state != application_state_t::IDLE)
  {
    spdlog::warn(
      "Player is not in IDLE state. Rejecting enable lightshow request!");
    return false;
  }

  application_state = application_state_t::LIGHTSHOW;
  application_state_changed(application_state);

  spdlog::info("Lightshow enabled");

  re->start();

  light_show_started();

  return true;
}

config_t::~config_t()
{
  locked_scope([this] { net.stop(); }, network_mtx);
}

config_t::config_t()
  : simulators()
  , net(tcp_port, udp_port)
{
  re = std::make_shared<render_engine>(1, 1);
  logger = spdlog::get("console");
  net.create_udp_connection();
}

void
config_t::load_from_file(std::string_view path)
{
  std::ifstream input_file(path.data());
  if (!input_file)
  {
    spdlog::warn(
      "Failed to open file {} Errno: {} ({}). Loading configuration failed!",
      path,
      errno,
      strerror(errno));
    return;
  }

  rapidjson::Document d;
  rapidjson::IStreamWrapper bsw(input_file);
  d.ParseStream(bsw);

  if (!d.IsObject() || !d.HasMember("display_configuration") ||
      !d["display_configuration"].IsObject())
  {
    spdlog::warn("Invalid or corrupted configuration file!");
    return;
  }

  rapidjson::Document::Object display_configuration_object =
    d["display_configuration"].GetObject();

  if (!display_configuration_object.HasMember("modules") ||
      !display_configuration_object["modules"].IsArray())
  {
    spdlog::warn("Invalid or corrupted configuration file!");
    return;
  }

  if (!display_configuration_object.HasMember("display_width") ||
      !display_configuration_object["display_width"].IsInt64() ||
      !display_configuration_object.HasMember("display_height") ||
      !display_configuration_object["display_height"].IsInt64())
  {
    spdlog::warn("Invalid or corrupted configuration file!");
    return;
  }

  this->set_resolution(
    display_configuration_object["display_width"].GetInt64(),
    display_configuration_object["display_height"].GetInt64());

  rapidjson::Document::Array attributes =
    display_configuration_object["modules"].GetArray();

  locked_scope(
    [&]
    {
      this->modules.clear();
      for (rapidjson::SizeType i = 0; i < attributes.Size(); i++)
      {
        modinfo module{};
        deserialize(module, attributes[i].GetObject());
        if (auto [module_it, inserted] =
              // Now module is trivially-copyale so move does not have any
              // effect but if it will become non-trivially-copyable in the
              // future then it will be good to have that move here
            this->modules.insert(std::move(module));
            !inserted)
        {
          spdlog::warn("Duplicated module uid[{}] will not be loaded to config",
                       module_it->base.uid.to_ascii());
        }
      }
      modules_config_container_rearanged();
    },
    this->modules_mtx);

  // TODO(all) deserialize rest of this structure
}

void
config_t::set_resolution(uresolution_t::underlying_type width,
                         uresolution_t::underlying_type height)
{
  std::vector<piwo::uid> modules_out_of_range;

  locked_scope(
    [&]
    {
      if (this->application_state != application_state_t::IDLE)
      {
        spdlog::warn(
          "Player is not in IDLE state. Rejecting resolution change!");
        return;
      }
      locked_scope(
        [&]
        {
          for (auto module : this->modules)
          {
            if (module.position.x >=
                  static_cast<ipos_t::underlying_type>(width) ||
                module.position.y >=
                  static_cast<ipos_t::underlying_type>(height))
            {
              modules_out_of_range.push_back(module.base.uid);
            }
          }
        },
        this->modules_mtx);

      for (auto module_out_of_range : modules_out_of_range)
      {
        ipos_t unassigned_position{ .x = MODULE_INVALID_POS,
                                    .y = MODULE_INVALID_POS };
        this->assign_module_position(module_out_of_range, unassigned_position);
      }

      locked_scope(
        [&]
        {
          this->resolution.width = width;
          this->resolution.height = height;
        },
        this->resolution_mtx);

      spdlog::info("New resolution {}x{}", width, height);
      event_loop::resize_data resize_packet{ width, height };
      _event_loop.send_event(resize_packet);
      config_resolution_changed();
    },
    this->application_state_mtx);
}

void
config_t::assign_module_position(const piwo::uid& uid, ipos_t& position)
{
  bool emit_data_changed_signal = false;
  if ((position.x < 0 && position.y >= 0) ||
      (position.y < 0 && position.x >= 0))
  {
    spdlog::warn("Inconsistent position x: {} y: {}. Rejecting assing module "
                 "position request!",
                 position.x,
                 position.y);
    return;
  }
  std::optional<piwo::uid> module_that_was_swapped;
  modinfo key{};
  key.base.uid = uid;

  locked_scope(
    [&]
    {
      if (this->application_state != application_state_t::IDLE)
      {
        spdlog::warn("Player is not in IDLE state. Rejecting assing module "
                     "position request!");
        return;
      }

      locked_scope(
        [&]
        {
          auto module_handle = this->modules.extract(key);

          if (!module_handle)
          {
            spdlog::error(
              "Failed to assign module position. UID[{:s}] not found.",
              uid.to_ascii());
            return;
          }

          if (position.x >= 0 && position.y >= 0)
          {
            for (auto module : this->modules)
            {
              if (module.position.x == position.x &&
                  module.position.y == position.y)
              {
                auto uid_module_to_swap_pos_with = module.base.uid;
                modinfo key_second{};
                key_second.base.uid = uid_module_to_swap_pos_with;
                auto module_handle_to_swap_pos_with =
                  this->modules.extract(key_second);
                if (!module_handle_to_swap_pos_with)
                {
                  spdlog::critical("This should never happen");
                  return;
                }

                module_handle_to_swap_pos_with.value().position =
                  module_handle.value().position;
                spdlog::info("Module uid[{}] assigned to pos x: {} y: {}",
                             module.base.uid.to_ascii(),
                             module_handle_to_swap_pos_with.value().position.x,
                             module_handle_to_swap_pos_with.value().position.y);
                this->modules.insert(std::move(module_handle_to_swap_pos_with));
                module_that_was_swapped = uid_module_to_swap_pos_with;
                break;
              }
            }
          }

          module_handle.value().position = position;
          this->modules.insert(std::move(module_handle));
          emit_data_changed_signal = true;
          spdlog::info("Module uid[{}] assigned to pos x: {} y: {}",
                       uid.to_ascii(),
                       position.x,
                       position.y);
        },
        this->modules_mtx); // locked scope
    },
    this->application_state_mtx); // locked scope

  if (emit_data_changed_signal)
  {
    if (module_that_was_swapped.has_value())
    {
      config_module_data_changed(*module_that_was_swapped);
    }
    config_module_data_changed(uid);
  }
}