# gui library that will be linked with rest application
cmake_minimum_required(VERSION 3.13)

set(LIB_UI qt_ui CACHE STRING "UI objects linked to core" FORCE)

set(CMAKE_AUTOUIC_SEARCH_PATHS forms)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

find_package(Qt5Core REQUIRED)
find_package(Qt5Widgets REQUIRED)
find_package(Qt5OpenGL REQUIRED)
find_package(Qt5Gui REQUIRED)
find_package(Qt5Quick REQUIRED)
find_package(OpenGL REQUIRED)
find_package(Qt5QuickCompiler REQUIRED)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_subdirectory(qml)
add_subdirectory(model)

qtquick_compiler_add_resources(QML_COMPILED_RESOURCES
  ${QML_RESOURCES}
)

set(PROJECT_SOURCES
  ${QML_COMPILED_RESOURCES}
  ${MODEL_SOURCES}
  qt_ui_iface.cpp
)

add_library(${LIB_UI} OBJECT
  ${PROJECT_SOURCES}
)

target_include_directories(
  ${LIB_UI} PUBLIC
  ${CMAKE_CURRENT_SOURCE_DIR}/include
  model/include
  qml
)

target_link_libraries(${LIB_UI} PUBLIC
  Qt5::Widgets
  Qt5::Core
  Qt5::Gui
  Qt5::Widgets
  Qt5::OpenGL
  Qt5::Quick
)