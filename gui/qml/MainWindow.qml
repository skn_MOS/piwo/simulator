
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQml.Models 2.12
import QtQuick.Layouts 1.12
import QtQuick.Dialogs 1.3

ApplicationWindow {
  id: mainWindow
  visible: true
  title: qsTr("P.I.W.O Player Simulator")

  palette.button: "#1d1d1d";
  palette.alternateBase: "black";
  palette.base: "#1d1d1d";
  palette.buttonText: "white";
  palette.dark: "#3e3e3e";
  palette.highlight: "white";
  palette.highlightedText: "black";
  palette.light: "#006ab1";
  palette.link: "blue";
  palette.linkVisited: "grey";
  palette.mid: "#006ab1";
  palette.midlight: "#606060";
  palette.shadow: "black";
  palette.text: "white";
  palette.toolTipBase: "white";
  palette.toolTipText: "black";
  palette.window: "#000000";
  palette.windowText: "white";

  function switchToLightMode() {
    palette.button = qsTr("#929292");
    palette.alternateBase = qsTr("black");
    palette.base = qsTr("#929292");
    palette.buttonText = qsTr("black");
    palette.dark = qsTr("#d4d4d4");
    palette.highlight = qsTr("black");
    palette.highlightedText = qsTr("white");
    palette.light = qsTr("#ca3333");
    palette.link = qsTr("blue");
    palette.linkVisited = qsTr("grey");
    palette.mid = qsTr("#ca3333");
    palette.midlight = qsTr("#ededed");
    palette.shadow = qsTr("black");
    palette.text = qsTr("black");
    palette.toolTipBase = qsTr("black");
    palette.toolTipText = qsTr("white");
    palette.window = qsTr("white");
    palette.windowText = qsTr("black");
  }

  function switchToDarkMode() {
    palette.button = qsTr("#1d1d1d");
    palette.alternateBase = qsTr("black");
    palette.base = qsTr("#1d1d1d");
    palette.buttonText = qsTr("white");
    palette.dark = qsTr("#3e3e3e");
    palette.highlight = qsTr("white");
    palette.highlightedText = qsTr("black");
    palette.light = qsTr("#006ab1");
    palette.link = qsTr("blue");
    palette.linkVisited = qsTr("grey");
    palette.mid = qsTr("#006ab1");
    palette.midlight = qsTr("#606060");
    palette.shadow = qsTr("black");
    palette.text = qsTr("white");
    palette.toolTipBase = qsTr("white");
    palette.toolTipText = qsTr("black");
    palette.window = qsTr("black");
    palette.windowText = qsTr("white");
  }

  menuBar: MainWindowMenuBar {
  }

  FileDialog {
    id: importDisplayConfigurationFileDialog
    height: 500
    width: 500
    title: "Read display configuration"
    folder: shortcuts.home
    nameFilters: [ "Display configuration files (*.json)" ]
    selectExisting: true
    selectFolder: false
    selectMultiple: false
    sidebarVisible: true
    onAccepted: {
      module_configuration_widget.import_configuration(importDisplayConfigurationFileDialog.fileUrls);
      importDisplayConfigurationFileDialog.close();
    }
  }

  header: TabBar {
    id: main_window_tap_bar
    spacing: 2

    MainWindowTabButton {
      mainWindowTabButtonTabName: qsTr("Simulator module config")
    }
    MainWindowTabButton {
      mainWindowTabButtonTabName: qsTr("Simulator")
    }
  }

  footer: MainWindowFooter {
    height: 25
  }

  ColumnLayout {
      anchors.fill: parent
    StackLayout {
      id: main_window_stack_layout
      currentIndex: main_window_tap_bar.currentIndex
      Layout.fillHeight: true
      Layout.fillWidth: true
      Layout.margins: 50

      DisplayConfiguration {

      }

      Simulator {

      }
    }
  }
}
