import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

import application_state_enum 1.0

Rectangle {
  color: palette.light
  RowLayout {
    anchors.fill: parent
    Text {
      Layout.alignment: Qt.AlignLeft
      Layout.leftMargin: 10
      verticalAlignment: Text.AlignVCenter
      Layout.fillHeight: true
      color: palette.text
      font.pointSize: 10
      font.family: "Ubuntu"
      text: {
        if(footer_model)
        {
          switch(footer_model.application_state) {
            case ApplicationStateEnum.IDLE:
              "Ready";
            break;
            case ApplicationStateEnum.DISPLAY_AUTO_CONFIG:
              "Display auto configuration in progress";
            break;
            case ApplicationStateEnum.LIGHTSHOW:
              "Lightshow is running";
            break;
            default:
            "Undefined";
          }
        }
        else
        {
          "Undefined";
        }
      }
    }

    Item {
      Layout.fillWidth: true
    }

    Text {
      Layout.alignment: Qt.AlignRight | Qt.AlignRight
      verticalAlignment: Text.AlignVCenter
      Layout.fillHeight: true
      color: palette.text
      font.pointSize: 10
      font.family: "Ubuntu"
      text: {
        if(footer_model &&
           footer_model.application_state == ApplicationStateEnum.LIGHTSHOW) {
          "Lightshow ON";
        }
        else {
          "Lightshow OFF"
        }
      }
    }

    FlatDesignSwitch {
      Layout.alignment: Qt.AlignVCenter | Qt.AlignRight
      Layout.preferredHeight: 17
      checkable: false
      checked: (footer_model &&
               footer_model.application_state == ApplicationStateEnum.LIGHTSHOW)

      onClicked: {
        footer_model.switch_lightshow();
      }
    }
  }
}
