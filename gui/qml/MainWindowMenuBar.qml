import QtQuick 2.12
import QtQuick.Controls 2.12

MenuBar {
  Menu {
    title: qsTr("File")
    Menu {
      title: qsTr("Import")
      Action {
        text: qsTr("Configuration");
        onTriggered: importDisplayConfigurationFileDialog.open();
      }
    }
    MenuSeparator { }
    Action {
      text: qsTr("Quit")
      onTriggered: Qt.callLater(Qt.quit);
    }
  }
  Menu {
    title: qsTr("Preferences")
    Menu {
      title: qsTr("Theme")
      Action {
        text: qsTr("Light mode")
        onTriggered: {
          mainWindow.switchToLightMode();
        }
      }
      Action {
        text: qsTr("Dark mode")
        onTriggered: {
          mainWindow.switchToDarkMode();
        }
      }
    }
  }
  Menu {
    title: qsTr("Help")
    Action { text: qsTr("About") }
  }
}
