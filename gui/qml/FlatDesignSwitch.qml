import QtQuick 2.12
import QtQuick.Controls 2.12

Switch {
  id: lightShowEnableSwitch
  property int switchWidth: 54
  property int switchHeight: 10
  property color onColor: palette.shadow
  property color offColor: palette.midlight
  property color onBorder: palette.shadow
  property color offBorder: palette.midlight
  property color dotColor: "white"
  property color dorBorder: "white"

  indicator: Rectangle {
    width: lightShowEnableSwitch.switchWidth
    height: parent.height
    radius: height / 2
    color: lightShowEnableSwitch.checked ? lightShowEnableSwitch.onColor : lightShowEnableSwitch.offColor
    border.width: 2
    border.color: lightShowEnableSwitch.checked ? lightShowEnableSwitch.onBorder : lightShowEnableSwitch.offBorder

    Rectangle {
      x: lightShowEnableSwitch.checked ? parent.width - width - 2 : 1
      width: lightShowEnableSwitch.checked ? parent.height - 4 : parent.height - 2
      height: width
      radius: width / 2
      anchors.verticalCenter: parent.verticalCenter
      color: lightShowEnableSwitch.dotColor
      border.color: lightShowEnableSwitch.dorBorder

      Behavior on x {
        NumberAnimation { duration: 200 }
      }
    }
  }
}