import QtQuick 2.12

import QtQuick.Controls 2.12
import QtQml.Models 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls.Styles 1.4

import "private"

RowLayout {

  Display {
    Layout.fillHeight: true
    Layout.fillWidth: true
  }
}
