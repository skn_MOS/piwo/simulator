import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

import "private"

ColumnLayout {
  width: parent.width
  height: parent.height
  property color tapButtonActiveColor: palette.dark
  SimulatorScene {
  }
}
