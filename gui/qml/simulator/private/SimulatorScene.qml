import QtQuick 2.12
import QtQuick.Window 2.15
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Shapes 1.15

ColumnLayout {
  id: simulatorSceneMainLayout

  Window {
    id: fullScreenWindow
    x: 0
    y: 0

    Rectangle {
      id: fullScreenArea
      anchors.fill: parent
      color: "transparent"
    }
  }

  RowLayout {
    Rectangle {
      id: displayContainer
      Layout.fillHeight: true
      Layout.fillWidth: true
      color: "transparent"

      Rectangle {
        color: palette.dark
        anchors.fill: parent
        Text {
          anchors.fill: parent
          text: "Full screen enabled"
          font.family: "Ubuntu"
          verticalAlignment: Text.AlignVCenter
          horizontalAlignment: Text.AlignHCenter
          font.pixelSize: 18
          color: palette.text
        }
      }
      Rectangle {
        id: mainDisplay
        anchors.fill: parent
        color: palette.dark
        state: "NORM"

        ColumnLayout {
          anchors.fill: parent
          Rectangle {
            id: renderArea
            Layout.fillHeight: true
            Layout.fillWidth: true
            color: palette.dark

            Image {
              id: image
              anchors.verticalCenter: parent.verticalCenter
              anchors.horizontalCenter: parent.horizontalCenter
              cache: false
              source: {
                if(simulator_scene_model &&
                   simulator_scene_model.active) {
                  simulator_scene_model.image_source + '/' + simulator_scene_model.dummy_frame;
                }
                else {
                  "";
                }
              }
              sourceSize.width: parent.width
              sourceSize.height: parent.height
            }
          }
        }
      }
    }
  }
}
