#include "qt_ui_iface.hpp"
#include <optional>

#include <QApplication>
#include <QQmlApplicationEngine>
#include <QString>
#include <QtWidgets>

#include "main_window.hpp"
#include "piwo.hpp"

void
start_gui(int argc, char* argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
  QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

  QApplication app(argc, argv);

  app.setOrganizationName("MOS");
  app.setOrganizationDomain("MOS");

  QQmlApplicationEngine engine;
  gui::main_window main_widget(engine);

  const QUrl url(QStringLiteral("qrc:/MainWindow.qml"));

  engine.addImageProvider(QLatin1String("piwo7"), new piwo7_image_provider);

  QObject::connect(
    &engine,
    &QQmlApplicationEngine::objectCreated,
    &app,
    [url](QObject* obj, const QUrl& obj_url)
    {
      if (!obj && url == obj_url)
        QCoreApplication::exit(-1);
    },
    Qt::QueuedConnection);
  engine.load(url);

  app.exec();
}

void
config_module_added(const piwo::uid uid)
{
  qt_ui_if.config_module_added(uid);
}

void
config_module_removed(const piwo::uid uid)
{
  qt_ui_if.config_module_removed(uid);
}

void
config_module_data_changed(const piwo::uid uid)
{
  qt_ui_if.config_module_data_changed(uid);
}

void
modules_config_container_rearanged()
{
  qt_ui_if.modules_config_container_rearanged();
}

void
config_resolution_changed()
{
  qt_ui_if.config_resolution_changed();
}

void
application_state_changed(const application_state_t state)
{
  qt_ui_if.application_state_changed(state);
}

void
light_show_started()
{
  qt_ui_if.light_show_started();
}

void
light_show_stopped()
{
  qt_ui_if.light_show_stopped();
}