#pragma once

#include <piwo/protodef.h>

#include <QObject>
#include <qqmlapplicationengine.h>

#include "common_types.hpp"
#include "config.hpp"

inline QQmlApplicationEngine* global_qml_engine;

class qt_ui_iface : public QObject
{
  Q_OBJECT

public:
  void
  config_module_added(const piwo::uid uid)
  {
    emit signal_config_module_added(uid);
  }

  void
  config_module_removed(const piwo::uid uid)
  {
    emit signal_config_module_removed(uid);
  }

  void
  config_module_data_changed(const piwo::uid uid)
  {
    emit signal_config_module_data_changed(uid);
  }

  void
  modules_config_container_rearanged()
  {
    emit signal_modules_config_container_rearanged();
  }

  void
  config_resolution_changed()
  {
    emit signal_config_resolution_changed();
  }

  void
  application_state_changed(const application_state_t state)
  {
    emit signal_application_state_changed(state);
  }

  void
  light_show_started()
  {
    emit signal_light_show_started();
  }

  void
  light_show_stopped()
  {
    emit signal_light_show_stopped();
  }

signals:
  void
  signal_config_module_added(const piwo::uid uid);

  void
  signal_config_module_removed(const piwo::uid uid);

  void
  signal_config_module_data_changed(const piwo::uid uid);

  void
  signal_modules_config_container_rearanged();

  void
  signal_config_resolution_changed();

  void
  signal_application_state_changed(const application_state_t state);

  void
  signal_light_show_started();

  void
  signal_light_show_stopped();

} inline qt_ui_if; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)
