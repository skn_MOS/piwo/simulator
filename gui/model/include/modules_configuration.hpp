#pragma once

#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QWidget>

#include "modules_matrix_model.hpp"

namespace gui
{
// NOLINTNEXTLINE(cppcoreguidelines-special-member-functions)
class modules_configuration : public QObject
{
  Q_OBJECT

public:
  explicit modules_configuration(QQmlContext* context,
                                 QWidget* parent = nullptr);
  ~modules_configuration() override = default;
public slots:
  void
  import_configuration(const QString& path);

private:
  modules_matrix_model _display_layout_model;
};
} // namespace gui
