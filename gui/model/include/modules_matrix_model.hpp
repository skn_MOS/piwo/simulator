#pragma once

#include <QAbstractListModel>

#include "config.hpp"
#include "modinfo.hpp"

namespace gui
{
// NOLINTNEXTLINE(cppcoreguidelines-special-member-functions)
class modules_matrix_model : public QAbstractItemModel
{
  Q_OBJECT
public:
  modules_matrix_model();
  ~modules_matrix_model() override = default;

  enum module_role
  {
    ID_ROLE = Qt::UserRole,
    UID_ROLE,
    LOGIC_ADDRESS_ROLE,
    GATEWAY_ID_ROLE,
    RADIO_ID_ROLE,
    STATIC_COLOR_ROLE,
    POSITION_X_ROLE,
    POSITION_Y_ROLE,
    IS_HIGHLIGHTED,
  };

  int
  rowCount(const QModelIndex& parent = QModelIndex()) const override;

  int
  columnCount(const QModelIndex& parent = QModelIndex()) const override;

  QModelIndex
  index(int row,
        int column,
        const QModelIndex& parent = QModelIndex()) const override;

  QVariant
  data(const QModelIndex& index, int role = Qt::DisplayRole) const override;

  QModelIndex
  parent(const QModelIndex& index) const override;

  bool
  setData(const QModelIndex& index,
          const QVariant& value,
          int role = Qt::EditRole) override;

  Qt::ItemFlags
  flags(const QModelIndex& index) const override;

  QHash<int, QByteArray>
  roleNames() const override;

public slots:
  void
  on_config_resolution_changed();

  void
  on_module_data_changed(const piwo::uid& uid);

  void
  on_modules_container_rearanged();

private:
  std::map<piwo::uid::packed_type, ipos_t> _modules_positions;
  std::vector<ipos_t> _highlighted_modules;
};

} // namespace gui
