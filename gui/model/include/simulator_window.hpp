#pragma once

#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QString>
#include <QWidget>

#include "simulator_scene_model.hpp"

#include "config.hpp"

namespace gui
{
// NOLINTNEXTLINE(cppcoreguidelines-special-member-functions)
class simulator_window : public QObject
{
  Q_OBJECT

public:
  explicit simulator_window(QQmlContext* context, QWidget* parent = nullptr);
  ~simulator_window() override = default;

private:
  simulator_scene_model _simulator_scene_model;
};
} // namespace gui
