#pragma once

#include <QObject>
#include <QString>
#include <QTimer>
#include <memory>

#include "common_types.hpp"
#include "config.hpp"

namespace gui
{
class simulator_scene_model : public QObject
{
  Q_OBJECT
  Q_PROPERTY(
    bool active READ get_simulator_state NOTIFY simulator_state_changed)

  Q_PROPERTY(int dummy_frame READ get_dummy_frame NOTIFY dummy_frame_changed)

  Q_PROPERTY(
    QString image_source READ get_image_source NOTIFY image_source_changed)

public:
  explicit simulator_scene_model(QObject* parent = nullptr);

  QString
  get_image_source();

  int
  get_dummy_frame();

  bool
  get_simulator_state();

public slots:

  void
  on_display_refresh();

  void
  on_light_show_stopped();

  void
  on_light_show_started();

signals:
  void
  simulator_state_changed(bool light_show_running);

  void
  image_source_changed(QString image_source);

  void
  dummy_frame_changed(int dummy_frame);

private:
  bool simulator_running = false;
  QString _image_source;
  int _dummy_frame = 0;

  std::unique_ptr<QTimer> _framerate_timer_up;
};
} // namespace gui
