#pragma once
#include "modules_configuration.hpp"

#include <QQmlApplicationEngine>

#include "footer_model.hpp"
#include "simulator_scene_model.hpp"

namespace gui
{
class main_window : public QObject
{
public:
  main_window(QQmlApplicationEngine& engine, QObject* p = nullptr);

private:
  modules_configuration configuration_widget;
  footer_model _footer_model;
  simulator_scene_model _simulator_scene_model;
};
};