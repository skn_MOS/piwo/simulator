#include "simulator_scene_model.hpp"
#include "qt_ui_iface.hpp"
#include <spdlog/spdlog.h>

namespace gui
{

simulator_scene_model::simulator_scene_model(QObject* parent)
  : QObject(parent)
  , _image_source("image://piwo7//pipeline")
{
  constexpr int timeout_ms = 50;

  this->_framerate_timer_up = std::make_unique<QTimer>(this);
  this->_framerate_timer_up->setInterval(timeout_ms);

  QObject::connect(&qt_ui_if,
                   &qt_ui_iface::signal_light_show_started,
                   this,
                   &simulator_scene_model::on_light_show_started);

  QObject::connect(&qt_ui_if,
                   &qt_ui_iface::signal_light_show_stopped,
                   this,
                   &simulator_scene_model::on_light_show_stopped);

  QObject::connect(_framerate_timer_up.get(),
                   &QTimer::timeout,
                   this,
                   &simulator_scene_model::on_display_refresh);

  emit this->simulator_state_changed(this->simulator_running);
}

QString
simulator_scene_model::get_image_source()
{
  return this->_image_source;
}

bool
simulator_scene_model::get_simulator_state()
{
  return this->simulator_running;
}

int
simulator_scene_model::get_dummy_frame()
{
  return this->_dummy_frame;
}

void
simulator_scene_model::on_light_show_stopped()
{
  spdlog::trace(__PRETTY_FUNCTION__);
  this->simulator_running = false;
  emit this->simulator_state_changed(this->simulator_running);

  this->_framerate_timer_up->stop();
}

void
simulator_scene_model::on_light_show_started()
{
  spdlog::trace(__PRETTY_FUNCTION__);
  this->simulator_running = true;
  this->_dummy_frame = 0;
  emit this->simulator_state_changed(this->simulator_running);

  this->_framerate_timer_up->start();
}

void
simulator_scene_model::on_display_refresh()
{
  // there we are doing a trict to force frame reload
  // in qml. This is neccessary cause QImageProvider reacts
  // only on path change, so to refresh the image
  // we are sending the path /pipeline/0 and /pipeline/1
  // interchangeably
  emit this->dummy_frame_changed(this->_dummy_frame);
  this->_dummy_frame ^= 1;
}
} // namespace gui
