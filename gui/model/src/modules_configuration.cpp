#include "modules_configuration.hpp"

#include <spdlog/spdlog.h>

#include <QDebug>
#include <QQmlApplicationEngine>

namespace gui
{
modules_configuration::modules_configuration(QQmlContext* context,
                                             QWidget* parent)
  : QObject(parent)
{
  qmlRegisterType<modules_matrix_model>(
    "display_layout_model", 1, 0, "DisplayLayoutModel");
  if (!context)
  {
    spdlog::error("Null ptr qml context");
    std::terminate();
  }
  context->setContextProperty(QStringLiteral("display_layout_model"),
                              &this->_display_layout_model);
}

void
modules_configuration::import_configuration(const QString& path)
{
  const auto c_string_path = QUrl(path).toLocalFile().toStdString();
  spdlog::info("Import configuration requested. PATH: {}", c_string_path);

  global_config.load_from_file(c_string_path);
}

} // namespace gui
