#include "footer_model.hpp"
#include "config.hpp"

Q_DECLARE_METATYPE(application_state_t);

namespace gui
{

footer_model::footer_model()
  : _application_state(static_cast<gui::cpp_enum::application_state_enum>(
      global_config.get_application_state()))
{
  qmlRegisterUncreatableMetaObject(gui::cpp_enum::staticMetaObject,
                                   "application_state_enum",
                                   1,
                                   0,
                                   "ApplicationStateEnum",
                                   "Error: only enums");
  int id = qRegisterMetaType<application_state_t>();

  QObject::connect(&qt_ui_if,
                   &qt_ui_iface::signal_application_state_changed,
                   this,
                   &footer_model::on_application_state_changed);

  emit this->application_state_changed(this->_application_state);
}

int
footer_model::get_application_state()
{
  return _application_state;
}

Q_INVOKABLE void
footer_model::switch_lightshow()
{
  spdlog::trace(__PRETTY_FUNCTION__);
  if (this->_application_state ==
      gui::cpp_enum::application_state_enum::LIGHTSHOW)
  {
    global_config.disable_light_show();
  }
  else
  {
    global_config.enable_light_show();
  }
}

void
footer_model::on_application_state_changed(const application_state_t state)
{
  this->_application_state =
    static_cast<gui::cpp_enum::application_state_enum>(state);
  emit this->application_state_changed(this->_application_state);
}

} // namespace gui
