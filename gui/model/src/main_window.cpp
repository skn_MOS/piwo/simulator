#include "main_window.hpp"

namespace gui
{
main_window::main_window(QQmlApplicationEngine& e, QObject* p)
  : QObject(p)
  , configuration_widget(e.rootContext())
{
  e.rootContext()->setContextProperty(
    QStringLiteral("module_configuration_widget"), &configuration_widget);

  e.rootContext()->setContextProperty(QStringLiteral("footer_model"),
                                      &_footer_model);
  e.rootContext()->setContextProperty(QStringLiteral("simulator_scene_model"),
                                      &this->_simulator_scene_model);
}
}