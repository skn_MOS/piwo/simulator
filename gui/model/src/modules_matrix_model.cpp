#include "modules_matrix_model.hpp"

#include "qt_ui_iface.hpp"
#include "typeutils.hpp"

namespace gui
{
modules_matrix_model::modules_matrix_model()
{
  QObject::connect(&qt_ui_if,
                   &qt_ui_iface::signal_config_resolution_changed,
                   this,
                   &modules_matrix_model::on_config_resolution_changed);
  QObject::connect(&qt_ui_if,
                   &qt_ui_iface::signal_config_module_data_changed,
                   this,
                   &modules_matrix_model::on_module_data_changed);
  QObject::connect(&qt_ui_if,
                   &qt_ui_iface::signal_modules_config_container_rearanged,
                   this,
                   &modules_matrix_model::on_modules_container_rearanged);

  beginResetModel();
  endResetModel();
}

int
modules_matrix_model::rowCount([[maybe_unused]] const QModelIndex& parent) const
{
  const auto& [mtx, resolution] = global_config.acquire_resolution();
  std::optional narrowed_height = narrow<int>(resolution.height);

  if (!narrowed_height.has_value())
  {
    spdlog::critical("Unsupported resolution height = {}", resolution.height);
    return 0;
  }
  return narrowed_height.value();
}

int
modules_matrix_model::columnCount(
  [[maybe_unused]] const QModelIndex& parent) const
{
  const auto& [mtx, resolution] = global_config.acquire_resolution();
  std::optional narrowed_width = narrow<int>(resolution.width);

  if (!narrowed_width.has_value())
  {
    spdlog::critical("Unsupported resolution width = {}", resolution.width);
    return 0;
  }
  return narrowed_width.value();
}

QModelIndex
modules_matrix_model::index(int row,
                            int column,
                            [[maybe_unused]] const QModelIndex& parent) const
{
  // Swapping attributes order is intentional
  // TODO(all) find out why orientation is flipped
  return this->createIndex(column, row);
}

QModelIndex
modules_matrix_model::parent([[maybe_unused]] const QModelIndex& index) const
{
  return QModelIndex();
}

QVariant
modules_matrix_model::data(const QModelIndex& index, int role) const
{
  if (!index.isValid())
    return QVariant();

  if (role == IS_HIGHLIGHTED)
  {
    return std::find_if(_highlighted_modules.begin(),
                        _highlighted_modules.end(),
                        [&index](const ipos_t& pos) {
                          return pos.x == index.row() &&
                                 pos.y == index.column();
                        }) != _highlighted_modules.end();
  }

  const auto& [mtx, modules] = global_config.acquire_modules();

  // TODO convert to std::find
  for (auto module : modules)
  {
    // Rows are vertical
    // Columns are horizontal
    // TODO(all) find out why orientation is flipped
    if (module.position.x == index.row() && module.position.y == index.column())
    {
      switch (role)
      {
        case ID_ROLE:
          return QVariant(qlonglong(module.base.id));
        case UID_ROLE:
          return QVariant(QString::fromStdString(module.base.uid.to_ascii()));
        case LOGIC_ADDRESS_ROLE:
          return QVariant(module.logic_address);
        case GATEWAY_ID_ROLE:
          return QVariant(module.gateway_id);
        case RADIO_ID_ROLE:
          return QVariant(module.radio_id);
        case STATIC_COLOR_ROLE:
          return QVariant(
            static_cast<unsigned int>(module.static_color.packed()));
        case POSITION_X_ROLE:
          return QVariant(module.position.x);
        case POSITION_Y_ROLE:
          return QVariant(module.position.y);
      }
      break;
    }
  }

  return QVariant();
}

bool
modules_matrix_model::setData([[maybe_unused]] const QModelIndex& index,
                              [[maybe_unused]] const QVariant& value,
                              [[maybe_unused]] int role)
{
  return true;
}

Qt::ItemFlags
modules_matrix_model::flags([[maybe_unused]] const QModelIndex& index) const
{
  return Qt::ItemIsDragEnabled;
}

QHash<int, QByteArray>
modules_matrix_model::roleNames() const
{
  QHash<int, QByteArray> names;
  names[ID_ROLE] = "id";
  names[UID_ROLE] = "uid";
  names[LOGIC_ADDRESS_ROLE] = "logic_address";
  names[GATEWAY_ID_ROLE] = "gateway_id";
  names[RADIO_ID_ROLE] = "radio_id";
  names[STATIC_COLOR_ROLE] = "static_color";
  names[POSITION_X_ROLE] = "position_x";
  names[POSITION_Y_ROLE] = "position_y";
  names[IS_HIGHLIGHTED] = "is_highlighted";
  return names;
}

void
modules_matrix_model::on_config_resolution_changed()
{
  beginResetModel();
  endResetModel();
}

void
modules_matrix_model::on_module_data_changed(const piwo::uid& uid)
{
  modinfo key{};
  key.base.uid = uid;
  const auto& [mtx, modules] = global_config.acquire_modules();
  auto module = modules.find(key);
  if (module == modules.end())
  {
    spdlog::warn("Failed to find uid[{}]. Model is now invalid",
                 uid.to_ascii());
    return;
  }

  if (this->_modules_positions.contains(uid.packed()))
  {
    auto old_position = this->_modules_positions[uid.packed()];
    emit this->dataChanged(this->index(old_position.x, old_position.y),
                           this->index(old_position.x, old_position.y));
  }

  if (module->position.x >= 0 && module->position.y >= 0)
  {
    emit this->dataChanged(this->index(module->position.x, module->position.y),
                           this->index(module->position.x, module->position.y));
    this->_modules_positions.insert_or_assign(uid.packed(), module->position);
  }
}

void
modules_matrix_model::on_modules_container_rearanged()
{
  this->beginResetModel();
  const auto& [mtx, modules] = global_config.acquire_modules();
  this->_highlighted_modules.clear();
  this->_modules_positions.clear();

  for (auto module : modules)
  {
    if (module.position.x >= 0 && module.position.y >= 0)
    {
      this->_modules_positions.insert_or_assign(module.base.uid,
                                                module.position);
    }
  }
  this->endResetModel();
}

} // namespace gui
