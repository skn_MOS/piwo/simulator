#include "simulator_window.hpp"

#include <spdlog/spdlog.h>

namespace gui
{
simulator_window::simulator_window(QQmlContext* context, QWidget* parent)
  : QObject(parent)
{
  if (!context)
  {
    spdlog::critical("Null ptr qml context");
    std::terminate();
  }
  context->setContextProperty(QStringLiteral("simulator_scene_model"),
                              &this->_simulator_scene_model);
}

} // namespace gui
